package com.matm.matmsdk.Adapters.Adapter;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;
import android.widget.TextView;

import androidx.recyclerview.widget.RecyclerView;

import com.matm.matmsdk.MPOS.PosActivity;
import isumatm.androidsdk.equitas.R;
import java.util.List;

public class QuickPaymentAdapter extends RecyclerView.Adapter<QuickPaymentAdapter.MyViewHolder>
  {
     private List<String> quickPaymentList;
     private PosActivity activity;
     private int prePosition=-1;  // store previous position

     //Constructor
      public QuickPaymentAdapter(List<String> quickPaymentList, PosActivity activity)
      {
          this.quickPaymentList = quickPaymentList;
          this.activity = activity;
      }

      //View holder for creating view
      public class MyViewHolder extends RecyclerView.ViewHolder
     {
         public TextView amnt_txt;
         private ImageView img_check7;
         private RelativeLayout btn_3000;

        public MyViewHolder( View view)
        {
            super(view);
            amnt_txt = view.findViewById(R.id.amnt_txt);
            img_check7 = view.findViewById(R.id.img_check7);
            btn_3000 = view.findViewById(R.id.btn_3000);

        }
    }

    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType)
    {
        View view = LayoutInflater.from(parent.getContext()).inflate(R.layout.quick_payment_row_layout,parent,false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position)
    {
        // Set View Operations
        holder.amnt_txt.setText(quickPaymentList.get(position));
        if (prePosition == position) {
            holder.itemView.setSelected(true);
            holder.img_check7.setImageResource(R.drawable.ic_check_circle_blue_24dp);
            activity.updateQuikPaymentAdapter(quickPaymentList.get(position));

        } else {
            holder.itemView.setSelected(false);
            holder.img_check7.setImageResource(R.drawable.ic_check_circle_grey_24dp);

        }
        holder.btn_3000.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (prePosition >= 0)
                notifyItemChanged(prePosition);
                prePosition = holder.getAdapterPosition();
                notifyItemChanged(prePosition);
            }
        });
    }
    @Override
    public int getItemCount()
    {
        return quickPaymentList.size();
    }
}
